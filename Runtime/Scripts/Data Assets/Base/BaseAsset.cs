﻿using System;

using UnityEngine;

using static UnityEngine.Debug;

namespace Vanilla.DataAssets
{

    [Serializable]
    public abstract class BaseAsset : ScriptableObject
    {

        [TextArea(minLines: 1,
                  maxLines: 50)]
        [SerializeField]
        private string _description;
        public string description => _description;

        public abstract void Broadcast();

        private void Awake()
        {
            #if DEBUG_DATA_ASSETS
            Log($"{name} - Awake");
            #endif
        }


        protected virtual void OnEnable()
        {
            #if DEBUG_DATA_ASSETS
            Log($"{name} - OnEnable");
            #endif
        }


        protected virtual void OnDisable()
        {
            #if DEBUG_DATA_ASSETS
            Log($"{name} - OnDisable");
            #endif
        }

    }

}