﻿using System;

using UnityEngine;
using UnityEngine.Events;

namespace Vanilla.DataAssets
{

	[Serializable]
	public abstract class GenericAsset<T> : BaseAsset
	{

		[SerializeField]
		protected T _value;
		public virtual T value
		{
			get => _value;
			set
			{
				#if DEBUG_DATA_ASSETS
				this.LogValueChange(from: _value,
				                    to: value);
				#endif

				var outgoing = _value;
				
				_value = value;

				Broadcast();
				
				onChange?.Invoke(arg0: outgoing, 
				                 arg1: _value);
			}
		}
		
		protected UnityAction<T> _onBroadcast;
		public UnityAction<T> onBroadcast
		{
			get => _onBroadcast;
			set
			{
				#if DEBUG_DATA_ASSETS
				if (!Quit.quitting)
				{
					this.LogSubscriptions(current: _onBroadcast,
					                      newState: value);
				}
				#endif

				_onBroadcast = value;
			}

		}
		
		protected UnityAction<T, T> _onChange;
		public UnityAction<T, T> onChange
		{
			get => _onChange;
			set
			{
				#if DEBUG_DATA_ASSETS
				if (!Quit.quitting)
				{
					this.LogSubscriptions(current: _onChange,
					                      newState: value);
				}
				#endif

				_onChange = value;
			}

		}


		protected virtual void OnValidate()
		{
			#if UNITY_EDITOR
			if (Application.isPlaying) value = _value;
			#endif
		}


		[ContextMenu(itemName: "Broadcast")]
		public override void Broadcast() => _onBroadcast?.Invoke(arg0: _value);

	}

}