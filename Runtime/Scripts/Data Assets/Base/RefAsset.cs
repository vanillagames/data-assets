﻿using System;

namespace Vanilla.DataAssets
{

    [Serializable]
    public abstract class RefAsset<D> : GenericAsset<D>
        where D : class { }

}