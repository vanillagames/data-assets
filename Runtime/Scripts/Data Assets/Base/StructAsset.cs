﻿using System;

namespace Vanilla.DataAssets
{

	[Serializable]
	public abstract class StructAsset<D> : GenericAsset<D>
		where D : struct { }

}