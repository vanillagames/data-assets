using System;

namespace Vanilla.DataAssets
{

	[Serializable]
	public abstract class ValueAsset<D> : GenericAsset<D>
		where D : unmanaged { }

}