﻿using System;

using UnityEngine;

namespace Vanilla.DataAssets
{

	[CreateAssetMenu(fileName = "New GameObject Asset",
	                 menuName = "Vanilla/Data Assets/Reference/GameObject")]
	[Serializable]
	public class GameObjectAsset : RefAsset<GameObject> { }

}