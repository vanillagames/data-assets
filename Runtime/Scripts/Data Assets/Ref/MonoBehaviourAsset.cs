﻿using System;

using UnityEngine;

namespace Vanilla.DataAssets
{

	[CreateAssetMenu(fileName = "New MonoBehaviour Asset",
	                 menuName = "Vanilla/Data Assets/Reference/MonoBehaviour")]
	[Serializable]
	public class MonoBehaviourAsset : RefAsset<MonoBehaviour> { }

}