﻿using System;

using UnityEngine;

namespace Vanilla.DataAssets
{

	[CreateAssetMenu(fileName = "New String Asset",
	                 menuName = "Vanilla/Data Assets/Reference/String")]
	[Serializable]
	public class StringAsset : RefAsset<string> { }

}