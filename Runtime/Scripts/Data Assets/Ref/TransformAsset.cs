using System;

using UnityEngine;

namespace Vanilla.DataAssets
{

	[CreateAssetMenu(fileName = "New Transform Asset",
	                 menuName = "Vanilla/Data Assets/Reference/Transform")]
	[Serializable]
	public class TransformAsset : RefAsset<Transform> { }

}