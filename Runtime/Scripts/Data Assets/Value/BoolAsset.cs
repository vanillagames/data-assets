﻿using System;

using UnityEngine;

namespace Vanilla.DataAssets
{

	[CreateAssetMenu(fileName = "New Bool Asset",
	                 menuName = "Vanilla/Data Assets/Value/Bool")]
	[Serializable]
	public class BoolAsset : ValueAsset<bool> { }

}