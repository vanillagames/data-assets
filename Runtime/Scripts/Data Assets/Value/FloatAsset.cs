﻿using System;

using UnityEngine;

namespace Vanilla.DataAssets
{

	[CreateAssetMenu(fileName = "New Float Asset",
	                 menuName = "Vanilla/Data Assets/Value/Float")]
	[Serializable]
	public class FloatAsset : ValueAsset<float> { }

}