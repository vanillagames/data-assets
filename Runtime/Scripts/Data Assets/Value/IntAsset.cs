﻿using System;

using UnityEngine;

namespace Vanilla.DataAssets
{

	[CreateAssetMenu(fileName = "New Int Data Asset",
	                 menuName = "Vanilla/Data Assets/Value/Int")]
	[Serializable]
	public class IntAsset : ValueAsset<int> { }

}