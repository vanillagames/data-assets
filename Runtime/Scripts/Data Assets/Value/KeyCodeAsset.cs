using System;

using UnityEngine;

namespace Vanilla.DataAssets
{
    [CreateAssetMenu(fileName = "New KeyCode Asset",
                     menuName = "Vanilla/Data Assets/Value/KeyCode")]
    [Serializable]
    public class KeyCodeAsset : ValueAsset<KeyCode>
    {

    }
}
