﻿using System;

using UnityEngine;

namespace Vanilla.DataAssets
{

	[CreateAssetMenu(fileName = "New Vector2 Asset",
	                 menuName = "Vanilla/Data Assets/Value/Vector2")]
	[Serializable]
	public class Vector2Asset : StructAsset<Vector2> { }

}