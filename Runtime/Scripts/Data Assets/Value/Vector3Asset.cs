﻿using System;

using UnityEngine;

namespace Vanilla.DataAssets
{

	[CreateAssetMenu(fileName = "New Vector3 Asset",
	                 menuName = "Vanilla/Data Assets/Value/Vector3")]
	[Serializable]
	public class Vector3Asset : StructAsset<Vector3> { }

}