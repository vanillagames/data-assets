using System;
using System.Collections;
using System.Collections.Generic;

using Cysharp.Threading.Tasks;

using UnityEngine;
using UnityEngine.Events;

namespace Vanilla.DataAssets
{
    
    public interface ISocket { }
    
    [Serializable]
    public abstract class GenericSocket<D, A> : ISocket
        where A : GenericAsset<D>
    {

        [SerializeField]
        protected A _asset;
        public A asset
        {
            get => _asset;
            set
            {
                if (ReferenceEquals(objA: _asset,
                                    objB: value)) return;

                if (AssetAssigned())
                {
                    _asset.onBroadcast -= onBroadcast;
                    _asset.onChange    -= onChange;
                }
                
                _asset = value;

                if (AssetAssigned())
                {
                    _asset.onBroadcast += onBroadcast;
                    _asset.onChange    += onChange;
                }
            }
        }

        [SerializeField]
        private D _fallback;
        public D fallback
        {
            get => _fallback;
            set
            {
                if (AssetAssigned())
                {
                    _fallback = value;

                    return;
                }
                
                if (Equals(objA: _fallback, 
                           objB: value)) return;

                var outgoing = _fallback;
                
                _fallback = value;
                
                onChange?.Invoke(arg0: outgoing, 
                                 arg1: _fallback);
                
                onBroadcast?.Invoke(arg0: _fallback);
            }
        }

        public UnityAction<D> onBroadcast;

        public UnityAction<D,D> onChange;


//        public void OnEnable()
//        {
//            if (AssetAssigned())
//            {
//                _asset.onBroadcast += onBroadcast;
//                _asset.onChange    += onChange;
//            }
//        }


//        public void OnDisable()
//        {
//            if (AssetAssigned())
//            {
//                _asset.onBroadcast -= onBroadcast;
//                _asset.onChange    -= onChange;
//            }
//        }
        
        public virtual D GetImmediate() => _asset != null ? _asset.value : fallback;
        
        public virtual async UniTask<D> Get() => _asset != null ? _asset.value : fallback;
        
        public virtual UniTask Set(D value)
        {
            if (AssetAssigned())
            {
                asset.value = value;
            }
            else
            {
                fallback = value;
            }

            return default;
        }


        public async UniTask Validate() => await Set(value: await Get());

        public string GetAssetName() => _asset != null ? _asset.name : DataAssets.Unknown;


        public bool AssetAssigned() => !ReferenceEquals(objA: _asset,
                                                        objB: null);

    }

}
