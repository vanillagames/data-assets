using System;

using Cysharp.Threading.Tasks;

using UnityEngine;

namespace Vanilla.DataAssets
{
	[Serializable]
	public class RefSocket<C, A> : GenericSocket<C, A>
		where C : class
		where A : RefAsset<C>
	{
//		[SerializeReference]
////		[SerializeField]
//		protected new RefAsset<C> _asset;
//		public new RefAsset<C> asset
//		{
//			get => _asset;
//			set => _asset = value;
//		}
	}
}