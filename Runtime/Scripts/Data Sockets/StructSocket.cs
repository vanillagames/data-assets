using System;

namespace Vanilla.DataAssets
{
	[Serializable]
	public class StructSocket<S, A> : GenericSocket<S, A>
		where S : struct
		where A : StructAsset<S>
	{

	}
}