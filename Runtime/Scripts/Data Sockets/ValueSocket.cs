using System;

namespace Vanilla.DataAssets
{
    [Serializable]
    public class ValueSocket<U, A> : GenericSocket<U, A>
        where U : unmanaged
        where A : ValueAsset<U>
    {

    }
}
