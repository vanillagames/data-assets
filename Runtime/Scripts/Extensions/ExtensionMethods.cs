﻿using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;

using static UnityEngine.Debug;

namespace Vanilla.DataAssets
{

	public static class ExtensionMethods
	{

		

		public static void LogSubscriptions(this BaseAsset input,
		                                    UnityAction        current,
		                                    UnityAction        newState) =>
			LogSubscriptions(targetName: input.name,
			                 currentInvocations: current?.GetInvocationList(),
			                 newInvocations: newState?.GetInvocationList());

		public static void LogSubscriptions<T>(this GenericAsset<T> input,
		                                       UnityAction<T>               current,
		                                       UnityAction<T>               newState) =>
			LogSubscriptions(targetName: input.name,
			                 currentInvocations: current?.GetInvocationList(),
			                 newInvocations: newState?.GetInvocationList());


		private static void LogSubscriptions(string                  targetName,
		                                     IReadOnlyList<Delegate> currentInvocations,
		                                     IReadOnlyList<Delegate> newInvocations)
		{
			bool newSubscription;

			Delegate action;

			// _onBroadcast always starts as null, which would mean we can't count it without an error.
			// If its null, we must be adding our first handler.
			if (currentInvocations == null)
			{
				newSubscription = true;

				action = newInvocations[index: newInvocations.Count - 1];
			}

			// If the newState is null... I have no idea? Does that happen in an unsubscription?
			else if (newInvocations == null)
			{
				newSubscription = false;

				action = currentInvocations[index: currentInvocations.Count - 1];
			}

			// Otherwise...
			else
			{
				// Count the amount of invocations on the outgoing list and the incoming list
				var oldDelCount = currentInvocations.Count;
				var newDelCount = newInvocations.Count;

				// If the incoming list is longer, it means this is a new event subscription.
				newSubscription = newDelCount > oldDelCount;

				// If its a new subscription, the action we want to log out is on the incoming list.
				// If its an un-subscription, the action we want is still available on the outgoing list.
				action = newSubscription ?
					         newInvocations[index: newDelCount     - 1] :
					         currentInvocations[index: oldDelCount - 1];
			}

			// Just a quick cache since we're going to access this string a fair few times.
			var targetString = action.Target.ToString();


			// Snip out the name of the GameObject, right at the start
			var gameObjectName = targetString.Substring(startIndex: 0,
			                                            length: targetString.IndexOf(value: '(') - 1);


			// Snip out the name of the Component, found right at the end of the targetString
			var indexOfLastPeriod = targetString.LastIndexOf(value: '(');

			var componentName = targetString.Substring(startIndex: indexOfLastPeriod + 1,
			                                           length: targetString.Length   - (indexOfLastPeriod + 2));

			// We can easily access the method name itself from the action.
			Log(message: $"Broadcast subscription event:\n\nFrame\t[{Time.frameCount}]\nScene\t[{GameObject.Find(name: gameObjectName).scene.name}]\nGameObject\t[{gameObjectName}]\nComponent\t[{componentName}]\nMethod\t\t[{action.Method.Name}]\n\n{(newSubscription ? "subscribed to" : "un-subscribed from")}\n\nData Asset\t\t[{targetName}]\n");

		}


		public static void LogValueChange<T>(this GenericAsset<T> input,
		                                     T                    from,
		                                     T                    to)
		{
			// We can easily access the method name itself from the action.
			Log(message: $"Value changed:\n\nFrame\t[{Time.frameCount}]\nAsset\t[{(input != null ? input.name : string.Empty)}]\nType\t[{(input != null ? typeof(T).Name : string.Empty)}]\nFrom\t[{(from != null? from.ToString() : string.Empty)}]\nTo\t[{(to != null? to.ToString() : string.Empty)}]\n");
		}

	}

}